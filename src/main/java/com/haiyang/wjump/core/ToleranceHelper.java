package com.haiyang.wjump.core;

/**
 * desc:宽容度工具类
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public class ToleranceHelper {
    /**
     * @param r
     * @param g
     * @param b
     * @param rt 目标R
     * @param gt 目标G
     * @param bt 目标B
     * @param t  宽容度
     * @return
     */
    public static boolean match(int r, int g, int b, int rt, int gt, int bt, int t) {
        return r > rt - t &&
                r < rt + t &&
                g > gt - t &&
                g < gt + t &&
                b > bt - t &&
                b < bt + t;
    }
}
