package com.haiyang.wjump.core;

import com.haiyang.wjump.bean.BoardPosition;
import com.haiyang.wjump.bean.Position;
import com.haiyang.wjump.config.JumpConfig;
import com.haiyang.wjump.strategy.JumpStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * desc:跳一跳helper
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public class JumpHelper {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * adbPath路径
     */
    private String adbPath;
    /**
     * jump系数
     */
    private Float jumpRatio;

    private static Random RANDOM = new Random();

    /**
     * @param adbPath   adbPath路径
     * @param jumpRatio jump系数
     */
    public JumpHelper(String adbPath, Float jumpRatio) {
        this.adbPath = adbPath;
        this.jumpRatio = jumpRatio;
    }


    /**
     * 自动jump
     *
     * @param jumpStrategy jump策略
     */
    public void autoJump(JumpStrategy jumpStrategy) {
        //1.截图
        //2.由截图找到棋子位置
        //3.由截图找到棋盘中心位置
        //4.由截图找到棋盘中心白点位置
        //5.计算距离
        //6.执行跳跃
        String rootPath = JumpHelper.class.getResource("/").getPath();
        File screenShotDir = new File(rootPath, JumpConfig.SCREEN_SHOT_PATH);

        if (!screenShotDir.exists()) {
            logger.info("screenShotDir 不存在，创建新的目录. 目录为:{}", screenShotDir.getAbsolutePath());
            screenShotDir.mkdirs();
        }
        logger.debug("screenShotDir: " + screenShotDir.getAbsolutePath());
        PositionSearcher chessPositionSearcher = new ChessPositionSearcher();
        BoardPositionSearcher boardPositionSearcher = new BoardPositionSearcher();
        BoardCenterPostionSearcher boardCenterPostionSearcher = new BoardCenterPostionSearcher();
        CurrentChessBoardPositionSearcher currentChessBoardPositionSearcher = new CurrentChessBoardPositionSearcher();
        ScoreSearcher scoreSearcher = new ScoreSearcher();
        int totalCount = 0;
        int centerHitCount = 0;
        for (int i = 0; i < 5000; i++) {
            try {
                totalCount++;
                logger.info("-----------第{}次跳开始-----------", totalCount);
                File file = new File(screenShotDir, i + ".png");
                if (file.exists()) {
                    file.deleteOnExit();
                }
                //进行截屏
                AdbTool.getInstance(adbPath).screenCapAndReturnImgFile(file);
                logger.debug("screenShot, filePath: {}", file.getAbsolutePath());
                //加入截屏图片
                BufferedImage image = ImgLoader.load(file.getAbsolutePath());
                Position chessPos = chessPositionSearcher.seach(image);
                //获取出当前成绩
                Integer currentScore = scoreSearcher.findCurrentScore(image);
                logger.info("当前成绩为:{}", currentScore);
                if (jumpStrategy.getShutdownScore() <= currentScore) {
                    logger.warn("当前成绩达到期望的成绩:{} ！本次自动JUMP将退出!下面由您手动跳吧!", jumpStrategy.getShutdownScore());
                    return;
                }
                if (chessPos != null) {
                    logger.info("本次的棋坐标为X为:{} Y为:{} ", chessPos.getX(), chessPos.getY());
                    boardPositionSearcher.setChessPos(chessPos);
                    BoardPosition nextCenter = boardPositionSearcher.seach(image);
                    if (nextCenter == null || nextCenter.getTopPosition().getX() == 0) {
                        logger.error("下一跳棋盘坐标搜索失败");
                        break;
                    } else {
                        int centerX, centerY;
                        Position whitePoint = boardCenterPostionSearcher.find(image, nextCenter.getTopPosition());
                        if (whitePoint != null) {
                            centerX = whitePoint.getX();
                            centerY = whitePoint.getY();
                            centerHitCount++;
                            logger.info("白点搜索到！白点坐标X为:{} Y为:{} 白点跳跃数为:{} 总跳跃数为:{}", centerX, centerY, centerHitCount, totalCount);
                        } else {
                            logger.info("未搜索到白点");
                            if (nextCenter.getLeftPosition().getX() != Integer.MAX_VALUE && nextCenter.getRightPosition().getX() != Integer.MIN_VALUE) {
                                //周围坐标搜索到后，X值为左右X均值,（XXX待优化->> Y值为左右Y值均值)
                                centerX = (nextCenter.getLeftPosition().getX() + nextCenter.getRightPosition().getX()) / 2;
                                centerY = (nextCenter.getLeftPosition().getY() + nextCenter.getRightPosition().getY()) / 2;
                            } else {
                                centerX = nextCenter.getTopPosition().getX();
                                centerY = nextCenter.getTopPosition().getY() + 48;
                            }

                            //TODO 每次跳跃时，记录这次打算跳跃棋盘的top和left坐标,以及中心点的左边距离与上边距;用来对跳跃系数进行调整
                        }
                        logger.info("下一跳棋盘坐标为X为:{}  Y为:{}", centerX, centerY);
                        int distance = (int) (Math.sqrt(Math.pow(centerX - chessPos.getX(), 2) + Math.pow((centerY - chessPos.getY()), 2)));
                        logger.info("本次跳跃距离为:{} ", distance);
                        logger.info("本次操作跳跃系数:{}", jumpRatio);
                        int swipDuration = (int) (distance * jumpRatio);
                        logger.info("本次跳跃按键时长为:{}", swipDuration);
                        //此处为重新开始的按钮位置,小米4C的为400,1500
                        int pressX = 400 + RANDOM.nextInt(100);
                        int pressY = 1500 + RANDOM.nextInt(100);
                        //是否画出扫描结果
                        if (logger.isDebugEnabled()) {
                            outputDebugJumpImg(rootPath, file, image, chessPos, nextCenter, centerX, centerY);
                        }
                        AdbTool.getInstance(adbPath).swipe(new int[]{pressX, pressY}, new int[]{pressX, pressY}, swipDuration);
                        logger.info("-----------第{}次跳结束-----------", totalCount);
                    }
                } else {
                    logger.error("棋子坐标搜索失败!");
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
            try {
                // sleep 随机时间，防止上传不了成绩
                Thread.sleep(4_000 + RANDOM.nextInt(4000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        logger.info("白点点击数为:{}  总跳跃数为:{} ", centerHitCount, totalCount);
    }

    /**
     * 输出调试图片
     *
     * @param chessPos   棋子坐标
     * @param nextCenter 搜索到的棋盘坐标
     * @param centerX    下一步将要JUMP的X坐标
     * @param centerY    下一步将要JUMP的Y坐标
     */
    private void outputDebugJumpImg(String rootPath, File file, BufferedImage image, Position chessPos, BoardPosition nextCenter, int centerX, int centerY) throws IOException {
        File jumpImgDebugDir = new File(rootPath, JumpConfig.JUMP_IMG_DEBUG_PATH);
        BufferedImage jumpDebugImg = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = jumpDebugImg.getGraphics();
        g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
        g.setColor(Color.RED);
        g.fillRect(chessPos.getX() - 5, chessPos.getY() - 5, 10, 10);
        g.setColor(Color.GREEN);
        g.fillRect(nextCenter.getTopPosition().getX() - 5, nextCenter.getTopPosition().getY() - 5, 10, 10);
        g.fillRect(nextCenter.getLeftPosition().getX() - 5, nextCenter.getLeftPosition().getY() - 5, 10, 10);
        g.fillRect(nextCenter.getRightPosition().getX() - 5, nextCenter.getRightPosition().getY() - 5, 10, 10);
        g.setColor(Color.BLUE);
        g.fillRect(centerX - 5, centerY - 5, 10, 10);
        File jumpDebugImgFile = new File(jumpImgDebugDir, file.getName());
        if (!jumpDebugImgFile.exists()) {
            jumpDebugImgFile.mkdirs();
            jumpDebugImgFile.createNewFile();
        }
        ImageIO.write(jumpDebugImg, "png", jumpDebugImgFile);
    }

}
