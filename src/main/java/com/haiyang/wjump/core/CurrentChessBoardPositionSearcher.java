package com.haiyang.wjump.core;

import com.haiyang.wjump.bean.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author haiyangp
 * date:  2018/1/14
 * desc: 当前棋子所在的棋盘坐标搜索
 */
public class CurrentChessBoardPositionSearcher {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * 搜索当前棋子所在的棋盘的左坐标
     *
     * @param chessPosition 棋子坐标
     * @return 棋盘左坐标
     */
    public Position findLeftPosition(Position chessPosition) {
        //TODO 待实现
        /**
         * 搜索棋子当前的棋盘边缘，获取出最左边缘坐标
         */
        return null;
    }


}
