package com.haiyang.wjump.core;

import com.haiyang.wjump.config.Constans;
import com.haiyang.wjump.config.JumpConfig;
import com.sun.istack.internal.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author haiyangp
 * date:  2018/1/13
 * desc: ADB 工具类
 */
public class AdbTool {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String adbPath;

    private AdbTool(String adbPath) {
        this.adbPath = adbPath;
    }

    private static AdbTool instance;

    /**
     * 获取adbTool
     *
     * @param adbPath adb目录
     * @return AdbTool
     */
    public static AdbTool getInstance(String adbPath) {
        if (instance == null) {
            instance = new AdbTool(adbPath);
        }
        return instance;
    }


    /**
     * 判断是否有在线的设备
     *
     * @return 是否有在线的设备
     */
    public boolean hasOnlineDevice() throws Exception {
        Process process = excuteCmd(Constans.CMD_ADB_DEVICE_LIST);
        String cmdResultStr = getProcessResultStr(process);
        logger.debug("excute adb cmd. result :\n{}", cmdResultStr);
        cmdResultStr = cmdResultStr.replace("List of devices attached", "");
        return cmdResultStr.contains("device");
    }


    /**
     * 截屏，并返回截图
     *
     * @param destFile 欲保存截图的文件
     * @return file
     */
    public File screenCapAndReturnImgFile(@NotNull File destFile) throws Exception {
        excuteCmd(Constans.CMD_ADB_SCREENCAP);
        excuteCmd(Constans.CMD_ADB_PULL_SCREENSHOT + destFile.getAbsolutePath());
        return destFile;
    }

    /**
     * 滑动
     *
     * @param startPos 开始坐标
     * @param endPos   结束坐标
     * @param duration 持续时间，毫秒ms
     */
    public void swipe(int[] startPos, int[] endPos, long duration) throws Exception {
        if (startPos.length != 2 || endPos.length != 2) {
            throw new RuntimeException("滑动异常，坐标不合法");
        }
        excuteCmd(String.format(Constans.CMD_ADB_SWIPE, startPos[0], startPos[1], endPos[0], endPos[1], duration));
    }


    /**
     * 获取命令执行结果字符串
     *
     * @return 结果字符串
     */
    private String getProcessResultStr(Process process) {
        try {
            BufferedReader br;
            br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 执行ADB命令
     *
     * @param cmd cmd
     * @return Process
     */
    private Process excuteCmd(String cmd) throws Exception {
        logger.debug("excute cmd is:{}", adbPath + " " + cmd);
        Process process = Runtime.getRuntime().exec(adbPath + " " + cmd);
        process.waitFor();
        return process;
    }


    public static void main(String[] args) {
        AdbTool adbTool = new AdbTool(JumpConfig.ADB_PATH);
        try {
            boolean b = adbTool.hasOnlineDevice();
            System.out.println(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
