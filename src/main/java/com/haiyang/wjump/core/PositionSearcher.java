package com.haiyang.wjump.core;

import com.haiyang.wjump.bean.Position;

import java.awt.image.BufferedImage;

/**
 * desc:位置搜索
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public interface PositionSearcher {
    /**
     * 搜索位置
     *
     * @param screenShotImg 屏幕截图
     * @return 坐标
     */
    Position seach(BufferedImage screenShotImg);
}
