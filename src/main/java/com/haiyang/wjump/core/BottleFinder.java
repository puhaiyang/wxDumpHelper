package com.haiyang.wjump.core;

import com.sun.istack.internal.NotNull;

import java.awt.image.BufferedImage;
import java.util.ArrayDeque;
import java.util.Queue;


/**
 * desc:瓶盖位置计算
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public class BottleFinder {

    public static final int TARGET = 255;

    /**
     * @param image 图片
     * @param topX  顶点坐标X
     * @param topY  顶点坐标Y
     */
    public int[] find(@NotNull BufferedImage image, int topX, int topY) {


        int[] ret = new int[6];
        ret[0] = topX;
        ret[1] = topY;
        ret[2] = Integer.MAX_VALUE;
        ret[3] = Integer.MAX_VALUE;
        ret[4] = Integer.MIN_VALUE;
        ret[5] = Integer.MAX_VALUE;

        int width = image.getWidth();
        int height = image.getHeight();

        boolean[][] vMap = new boolean[width][height];
        Queue<int[]> queue = new ArrayDeque<>();
        int[] pos = {topX, topY};
        queue.add(pos);

        while (!queue.isEmpty()) {
            pos = queue.poll();
            topX = pos[0];
            topY = pos[1];
            //过滤掉超过图片大小坐标和已经扫描过的坐标点
            if (topX < 0 || topX >= width || topY < 0 || topY > height || vMap[topX][topY]) {
                continue;
            }
            vMap[topX][topY] = true;
            int pixel = image.getRGB(topX, topY);
            int r = (pixel & 0xff0000) >> 16;
            int g = (pixel & 0xff00) >> 8;
            int b = (pixel & 0xff);
            //如果这一点为白点
            if (r == TARGET && g == TARGET && b == TARGET) {
                //让topX最左
                if (topX < ret[2]) {
                    ret[2] = topX;
                    ret[3] = topY;
                }
                //让topY最上
                else if (topX == ret[2] && topY < ret[3]) {
                    ret[2] = topX;
                    ret[3] = topY;
                }
                //让topX最右
                if (topX > ret[4]) {
                    ret[4] = topX;
                    ret[5] = topY;
                }
                //最topY最上
                else if (topX == ret[4] && topY < ret[5]) {
                    ret[4] = topX;
                    ret[5] = topY;
                }
                //让topY最上
                if (topY < ret[1]) {
                    ret[0] = topX;
                    ret[1] = topY;
                }
                queue.add(buildArray(topX - 1, topY));
                queue.add(buildArray(topX + 1, topY));
                queue.add(buildArray(topX, topY - 1));
                queue.add(buildArray(topX, topY + 1));
            }
        }

        return ret;
    }

    public static int[] buildArray(int i, int j) {
        int[] ret = {i, j};
        return ret;
    }


}
