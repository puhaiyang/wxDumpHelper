package com.haiyang.wjump.core;

import com.haiyang.wjump.bean.Position;
import com.sun.istack.internal.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.util.ArrayDeque;
import java.util.Queue;

/**
 * desc:棋盘中心点搜索，即白点
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public class BoardCenterPostionSearcher {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    public static final int TARGET = 245;

    /**
     * 在某一坐标区域内搜索白点
     *
     * @param image       图片
     * @param topPosition 棋盘顶点坐标
     * @return 白点坐标
     */
    public Position find(@NotNull BufferedImage image, Position topPosition) {
        int width = image.getWidth();
        int height = image.getHeight();

        int leftX = Integer.max(topPosition.getX() - 120, 0);
        int rightX = Integer.min(topPosition.getX() + 120, width - 1);
        int topY = Integer.max(topPosition.getY(), 0);
        int bottomY = Integer.min(topPosition.getY() + 180, height - 1);

        for (int i = leftX; i <= rightX; i++) {
            for (int j = topY; j <= bottomY; j++) {
                int pixel = image.getRGB(i, j);
                int r = (pixel & 0xff0000) >> 16;
                int g = (pixel & 0xff00) >> 8;
                int b = (pixel & 0xff);
                if (r == TARGET && g == TARGET && b == TARGET) {
                    boolean[][] vMap = new boolean[width][height];
                    Queue<int[]> queue = new ArrayDeque<>();
                    int[] pos = {i, j};
                    queue.add(pos);
                    int maxX = Integer.MIN_VALUE;
                    int minX = Integer.MAX_VALUE;
                    int maxY = Integer.MIN_VALUE;
                    int minY = Integer.MAX_VALUE;
                    while (!queue.isEmpty()) {
                        pos = queue.poll();
                        int x = pos[0];
                        int y = pos[1];
                        if (x < leftX || x > rightX || y < topY || y > bottomY || vMap[x][y]) {
                            continue;
                        }
                        vMap[x][y] = true;
                        pixel = image.getRGB(x, y);
                        r = (pixel & 0xff0000) >> 16;
                        g = (pixel & 0xff00) >> 8;
                        b = (pixel & 0xff);
                        if (r == TARGET && g == TARGET && b == TARGET) {
                            maxX = Integer.max(maxX, x);
                            minX = Integer.min(minX, x);
                            maxY = Integer.max(maxY, y);
                            minY = Integer.min(minY, y);
                            queue.add(buildArray(x - 1, y));
                            queue.add(buildArray(x + 1, y));
                            queue.add(buildArray(x, y - 1));
                            queue.add(buildArray(x, y + 1));
                        }
                    }

                    logger.debug("whitePoint: " + maxX + ", " + minX + ", " + maxY + ", " + minY);
                    //如果搜索到的圆点坐标符合白点的大小，则为正确的圆点坐标
                    if (maxX - minX <= 45 && maxX - minX >= 35 && maxY - minY <= 30 && maxY - minY >= 20) {
                        int[] ret = {(minX + maxX) / 2, (minY + maxY) / 2};
                        return new Position(ret[0], ret[1]);
                    } else {
                        return null;
                    }

                }
            }
        }
        return null;
    }

    public static int[] buildArray(int i, int j) {
        int[] ret = {i, j};
        return ret;
    }
}
