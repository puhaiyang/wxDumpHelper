package com.haiyang.wjump.core;

import com.haiyang.wjump.bean.BoardPosition;
import com.haiyang.wjump.bean.Position;
import com.sun.istack.internal.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

/**
 * desc:棋盘位置搜索
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public class BoardPositionSearcher {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private BottleFinder bottleFinder = new BottleFinder();

    private Position chessPos;

    public Position getChessPos() {
        return chessPos;
    }

    public void setChessPos(Position chessPos) {
        this.chessPos = chessPos;
    }

    public BoardPosition seach(@NotNull BufferedImage screenShotImg) {
        if (screenShotImg == null) {
            return null;
        }
        BoardPosition boardPosition = new BoardPosition();

        int width = screenShotImg.getWidth();
        int height = screenShotImg.getHeight();
        //先获取出0 200这一点的像素,即顶部某的一点
        int pixel = screenShotImg.getRGB(0, 200);
        int r1 = (pixel & 0xff0000) >> 16;
        int g1 = (pixel & 0xff00) >> 8;
        int b1 = (pixel & 0xff);
        Map<Integer, Integer> map = new HashMap<>();
        //一列一列地搜索,在map中放入遍历点像素在这一行中出现的次数
        for (int i = 0; i < width; i++) {
            pixel = screenShotImg.getRGB(i, height - 1);
            map.put(pixel, map.getOrDefault(pixel, 0) + 1);
        }
        //获取出存在于map中像素出现次数最多的像素
        int max = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() > max) {
                pixel = entry.getKey();
                max = entry.getValue();
            }
        }
        int r2 = (pixel & 0xff0000) >> 16;
        int g2 = (pixel & 0xff00) >> 8;
        int b2 = (pixel & 0xff);
        //获取出游戏背景从顶到底的颜色RGB值
        int t = 16;

        int minR = Integer.min(r1, r2) - t;
        int maxR = Integer.max(r1, r2) + t;
        int minG = Integer.min(g1, g2) - t;
        int maxG = Integer.max(g1, g2) + t;
        int minB = Integer.min(b1, b2) - t;
        int maxB = Integer.max(b1, b2) + t;

        logger.trace(minR + ", " + minG + ", " + minB);
        logger.trace(maxR + ", " + maxG + ", " + maxB);

        int[] ret = new int[6];
        int targetR = 0, targetG = 0, targetB = 0;
        boolean found = false;
        //遍历寻找棋盘顶点坐标，在棋子坐标上方搜索，从游戏背景4分之一处开始搜索，提高速度
        for (int backgroundY = height / 4; backgroundY < chessPos.getY(); backgroundY++) {
            for (int backgroundX = 0; backgroundX < width; backgroundX++) {
                int dx = Math.abs(backgroundX - chessPos.getX());
                int dy = Math.abs(backgroundY - chessPos.getY());
                if (dy > dx) {
                    //如果这一点到棋子X的距离比这一点到棋子Y的距离小,就跳出循环， WHY？
                    continue;
                }
                //获取出扫描这一点的RGB值
                pixel = screenShotImg.getRGB(backgroundX, backgroundY);
                int r = (pixel & 0xff0000) >> 16;
                int g = (pixel & 0xff00) >> 8;
                int b = (pixel & 0xff);
                //如果这一点的RGB值不在屏幕背景色的区间内
                if (r < minR || r > maxR || g < minG || g > maxG || b < minB || b > maxB) {
                    ret[0] = backgroundX;
                    ret[1] = backgroundY;
                    //则下一步的顶点坐标为这个点
                    logger.trace("top, x: " + backgroundX + ", y: " + backgroundY);
                    //遍历这个点向下5个高度的像素
                    for (int k = 0; k < 5; k++) {
                        pixel = screenShotImg.getRGB(backgroundX, backgroundY + k);
                        targetR += (pixel & 0xff0000) >> 16;
                        targetG += (pixel & 0xff00) >> 8;
                        targetB += (pixel & 0xff);
                    }
                    //取出这个点的像素平均值
                    targetR /= 5;
                    targetG /= 5;
                    targetB /= 5;
                    found = true;
                    break;
                }
            }
            if (found) {
                break;
            }
        }

        //T判断是否为瓶盖
        if (targetR == BottleFinder.TARGET && targetG == BottleFinder.TARGET && targetB == BottleFinder.TARGET) {
            ret = bottleFinder.find(screenShotImg, ret[0], ret[1]);
            logger.debug("发现瓶盖了！！！！");
            return setBoardPosition(boardPosition, ret);
        }

        boolean[][] matchMap = new boolean[width][height];
        boolean[][] vMap = new boolean[width][height];
        ret[2] = Integer.MAX_VALUE;
        ret[3] = Integer.MAX_VALUE;
        ret[4] = Integer.MIN_VALUE;
        ret[5] = Integer.MAX_VALUE;

        Queue<int[]> queue = new ArrayDeque<>();
        queue.add(ret);
        while (!queue.isEmpty()) {
            int[] item = queue.poll();
            int i = item[0];
            int j = item[1];
            if (j >= chessPos.getY()) {
//                已搜索到棋子的Y值位置了，结束本次搜索，跳出循环
                continue;
            }

            if (i < Integer.max(ret[0] - 300, 0) || i >= Integer.min(ret[0] + 300, width) || j < Integer.max(0, ret[1] - 400) || j >= Integer.max(height, ret[1] + 400) || vMap[i][j]) {
//对于距离棋子坐标太远的跳出循环(即棋子坐标左、右、上、下的坐标),以及已经搜索过的坐标也跳出循环
                continue;
            }
            vMap[i][j] = true;
            pixel = screenShotImg.getRGB(i, j);
            int r = (pixel & 0xff0000) >> 16;
            int g = (pixel & 0xff00) >> 8;
            int b = (pixel & 0xff);
            //将每一个坐标点与棋盘顶点的RGB值比较
            matchMap[i][j] = ToleranceHelper.match(r, g, b, targetR, targetG, targetB, 16);
            if (i == ret[0] && j == ret[1]) {
                logger.trace(matchMap[i][j] + "");
            }
            //如果在棋盘面上
            if (matchMap[i][j]) {
                //获取出最左边的棋盘坐标
                if (i < ret[2]) {
                    ret[2] = i;
                    ret[3] = j;
                }//获取最上的棋盘坐标
                else if (i == ret[2] && j < ret[3]) {
                    ret[2] = i;
                    ret[3] = j;
                }
                //获取出最右边的棋盘坐标
                if (i > ret[4]) {
                    ret[4] = i;
                    ret[5] = j;
                }//获取出最上的棋盘坐标
                else if (i == ret[4] && j < ret[5]) {
                    ret[4] = i;
                    ret[5] = j;
                }
                //获取出最上的坐标点
                if (j < ret[1]) {
                    ret[0] = i;
                    ret[1] = j;
                }
                //将目标点左右上下的点放入队列中,实现递归
                queue.add(buildArray(i - 1, j));
                queue.add(buildArray(i + 1, j));
                queue.add(buildArray(i, j - 1));
                queue.add(buildArray(i, j + 1));
            }
        }

        logger.trace("left, x: " + ret[2] + ", y: " + ret[3]);
        logger.trace("right, x: " + ret[4] + ", y: " + ret[5]);

        //由三点计算中点坐标
//        boardPosition.setRightPosition();
        return setBoardPosition(boardPosition, ret);
    }

    private BoardPosition setBoardPosition(BoardPosition boardPosition, int[] ret) {
        Position topPosition = new Position(ret[0], ret[1]);
        Position leftPosition = new Position(ret[2], ret[3]);
        Position rightPosition = new Position(ret[4], ret[5]);
        boardPosition.setTopPosition(topPosition);
        boardPosition.setLeftPosition(leftPosition);
        boardPosition.setRightPosition(rightPosition);
        return boardPosition;
    }

    private int[] buildArray(int i, int j) {
        int[] ret = {i, j};
        return ret;
    }

}
