package com.haiyang.wjump.core;

import com.haiyang.wjump.bean.ColorRGB;
import com.haiyang.wjump.bean.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author haiyangp
 * date:  2018/1/21
 * desc: 成绩搜索
 */
public class ScoreSearcher {
    private Logger logger = LoggerFactory.getLogger(ScoreSearcher.class);

    private int startY = 200;
    /**
     * 成绩的起点坐标,小米4C的为125,205;结束点为190,288
     */
    private Position scoreStartPosition = new Position(122, startY);
    //成绩图象宽度
    private Integer scoreWidth = 193 - scoreStartPosition.getX();
    //成绩图象高度
    private Integer scoreHeight = 293 - 200;
    //每一个数字的宽度步长
    private Integer scoreWidthStep = 8;
    //成绩字体的RGB值
    private ColorRGB scoreColor = new ColorRGB(81, 75, 73);
    //02356的像素统计
    private Integer RGB_0_2_3_5_6_COUNT = 3911;

    private static Map<Integer, Integer> integerMap = new HashMap<Integer, Integer>(10);

    static {
        integerMap.put(10000, 0);//构造的
        integerMap.put(1600, 1);
        integerMap.put(2000, 2);//构造的
        integerMap.put(3000, 3);//构造的
        integerMap.put(3061, 4);
        integerMap.put(5000, 5);//构造的
        integerMap.put(6000, 6);//构造的
        integerMap.put(2178, 7);
        integerMap.put(4472, 8);
        integerMap.put(3894, 9);
    }

    /**
     * 搜索当前分数
     *
     * @param screenShotImg 截图
     * @return 当前分数
     */
    public Integer findCurrentScore(BufferedImage screenShotImg) {
        int startX = scoreStartPosition.getX();
        int startY;
        Map<Integer, Integer> scoreColorCountMap = new HashMap<>(4);
        //搜索4个数字
        for (int i = 0; i < 4; i++) {
            if (i != 0) {
                startX = startX + scoreWidthStep;
            }
            int endX = startX + scoreWidth;
//            logger.debug("indexI:{}  startX:{} endX:{}", i, startX, endX);
            int colorCount = 0;
            for (; startX <= endX; startX++) {
                startY = scoreStartPosition.getY();
                int endY = startY + scoreHeight;
//                logger.debug("indexI:{} startY:{} endY:{}", i, startY, endY);
                for (; startY <= endY; startY++) {
                    //遍历每一个像素
                    int pixel = screenShotImg.getRGB(startX, startY);
                    int r = (pixel & 0xff0000) >> 16;
                    int g = (pixel & 0xff00) >> 8;
                    int b = (pixel & 0xff);
                    if (scoreColor.isMatch(r, g, b)) {
                        colorCount++;
                    }
                }
                //写个责任链、处理3911的数
                if (colorCount == RGB_0_2_3_5_6_COUNT) {
                    //画4个点，再区分02356
                    boolean[] abcdArray = getABCD(screenShotImg, endX, endY);
                    if (logger.isDebugEnabled()) {
                        for (boolean abcdItem : abcdArray) {
                            logger.trace(abcdItem + "");
                        }
                    }
                    if (abcdArray[0] && abcdArray[1] && abcdArray[2] && abcdArray[3]) {
                        //为ABCD 0
                        colorCount = 10000;
                    } else if (abcdArray[0] && abcdArray[2] && abcdArray[3]) {
                        //为ACD 6
                        colorCount = 6000;
                    } else if (abcdArray[1] && abcdArray[3]) {
                        //为BD 3
                        colorCount = 3000;
                    } else if (abcdArray[1] && abcdArray[2]) {
                        //为BC 2
                        colorCount = 2000;
                    } else if (abcdArray[0] && abcdArray[3]) {
                        //为AD 5
                        colorCount = 5000;
                    } else {
                        logger.error("未知的数字");
                    }
                }
            }
            scoreColorCountMap.put(i, colorCount);
        }
        logger.trace("成绩颜色统计数据为:{}", scoreColorCountMap.toString());
        int resultInt = 0;
        for (int i = 0; i < 4; i++) {
            Integer value = integerMap.get(scoreColorCountMap.get(i));
            logger.trace(i + "  " + value);
            if (value != null) {
                resultInt = (resultInt * 10) + value;

            } else {
                return resultInt;
            }
        }
        return resultInt;
    }

    /**
     * 画ABCD四个点
     *
     * @param endX endX
     * @param endY endY
     * @return abcdArray; if exist,then return true
     */
    private boolean[] getABCD(BufferedImage screenShotImg, int endX, int endY) {
        boolean[] abcdArray = new boolean[4];

        int firstY = startY + (scoreHeight / 4);
        int secondY = startY + (scoreHeight * 2 / 3);

        int startX = endX - scoreWidth;
        for (int i = endX; i > startX; i--) {
            //firstLine search
            int firstLinepixel = screenShotImg.getRGB(i, firstY);
            int firstLineR = (firstLinepixel & 0xff0000) >> 16;
            int firstLineG = (firstLinepixel & 0xff00) >> 8;
            int firstLineB = (firstLinepixel & 0xff);
            if (scoreColor.isMatch(firstLineR, firstLineG, firstLineB)) {
                if (i > (startX + endX) / 2) {
                    //为后半,则为B
                    abcdArray[1] = true;
                } else {
                    //为前半，则为A
                    abcdArray[0] = true;
                }
            }
            //secondLine search
            int secondLinepixel = screenShotImg.getRGB(i, secondY);
            int secondLineR = (secondLinepixel & 0xff0000) >> 16;
            int secondLineG = (secondLinepixel & 0xff00) >> 8;
            int secondLineB = (secondLinepixel & 0xff);
            if (scoreColor.isMatch(secondLineR, secondLineG, secondLineB)) {
                if (i > (startX + endX) / 2) {
                    //为后半,则为D
                    abcdArray[3] = true;
                } else {
                    //为前半，则为C
                    abcdArray[2] = true;
                }
            }
        }
        return abcdArray;
    }

    public static void main(String[] args) {
        ScoreSearcher scoreSearcher = new ScoreSearcher();
        try {
            for (int i = 0; i < 15; i++) {
                BufferedImage image = ImgLoader.load("E:\\idea_space\\wxDumpHelper\\target\\classes\\imgs\\jumpImgDebug\\" + i + ".png");
                Integer currentScore = scoreSearcher.findCurrentScore(image);
                System.out.println(i + "=" + currentScore);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
