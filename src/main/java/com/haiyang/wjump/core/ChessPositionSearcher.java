package com.haiyang.wjump.core;

import com.haiyang.wjump.bean.Position;
import com.sun.istack.internal.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;

/**
 * desc:棋子位置搜索
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public class ChessPositionSearcher implements PositionSearcher {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    public static final int R_TARGET = 40;

    public static final int G_TARGET = 43;

    public static final int B_TARGET = 86;

    @Override
    public Position seach(@NotNull BufferedImage screenShotImg) {

        int width = screenShotImg.getWidth();
        int height = screenShotImg.getHeight();

        int maxX = Integer.MIN_VALUE;
        int minX = Integer.MAX_VALUE;
        int maxY = Integer.MIN_VALUE;
        int minY = Integer.MAX_VALUE;
        for (int i = 0; i < width; i++) {
            for (int j = height / 4; j < height * 3 / 4; j++) {
                int pixel = screenShotImg.getRGB(i, j);
                int r = (pixel & 0xff0000) >> 16;
                int g = (pixel & 0xff00) >> 8;
                int b = (pixel & 0xff);
                if (ToleranceHelper.match(r, g, b, R_TARGET, G_TARGET, B_TARGET, 16)) {
                    maxX = Integer.max(maxX, i);
                    minX = Integer.min(minX, i);
                    maxY = Integer.max(maxY, j);
                    minY = Integer.min(minY, j);
                }
            }
        }
        Position position = new Position();
        position.setX((maxX + minX) / 2 + 3);
        position.setY(maxY);
        return position;
    }
}
