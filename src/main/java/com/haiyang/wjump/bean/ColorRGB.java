package com.haiyang.wjump.bean;

/**
 * @author haiyangp
 * date:  2018/1/21
 * desc: 颜色RGB值
 */
public class ColorRGB {

    private int r;
    private int g;
    private int b;

    public ColorRGB() {
    }

    public ColorRGB(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    /**
     * 是否与当前RGB匹配
     *
     * @param srcR 待比较的R
     * @param srcG 待比较的G
     * @param srcB 待比较的B
     */
    public boolean isMatch(int srcR, int srcG, int srcB) {
        return srcR == r && srcG == g && srcB == b;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
