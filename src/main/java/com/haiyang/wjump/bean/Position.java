package com.haiyang.wjump.bean;

/**
 * @author haiyangp
 * date:  2018/1/13
 * desc: 某点的位置
 */
public class Position {
    /**
     * X坐标
     */
    private int x = 0;
    /**
     * y坐标
     */
    private int y = 0;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position() {
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
