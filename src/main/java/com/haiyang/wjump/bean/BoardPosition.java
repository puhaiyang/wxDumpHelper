package com.haiyang.wjump.bean;

/**
 * @author haiyangp
 * date:  2018/1/14
 * desc: 棋盘坐标
 */
public class BoardPosition {

    private Position topPosition;
    private Position leftPosition;
    private Position rightPosition;
    private Position centerPositino;

    public Position getTopPosition() {
        return topPosition;
    }

    public void setTopPosition(Position topPosition) {
        this.topPosition = topPosition;
    }

    public Position getLeftPosition() {
        return leftPosition;
    }

    public void setLeftPosition(Position leftPosition) {
        this.leftPosition = leftPosition;
    }

    public Position getRightPosition() {
        return rightPosition;
    }

    public void setRightPosition(Position rightPosition) {
        this.rightPosition = rightPosition;
    }

    public Position getCenterPositino() {
        return centerPositino;
    }

    public void setCenterPositino(Position centerPositino) {
        this.centerPositino = centerPositino;
    }
}
