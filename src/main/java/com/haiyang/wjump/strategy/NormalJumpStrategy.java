package com.haiyang.wjump.strategy;

/**
 * desc: 普通jump策略
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public class NormalJumpStrategy implements JumpStrategy {
    private int maxSocre;

    public NormalJumpStrategy(int maxSocre) {
        this.maxSocre = maxSocre;
    }

    @Override
    public int getShutdownScore() {
        return maxSocre;
    }
}
