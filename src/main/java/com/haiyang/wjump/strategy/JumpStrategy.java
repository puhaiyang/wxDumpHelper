package com.haiyang.wjump.strategy;

/**
 * desc:jump策略
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public interface JumpStrategy {

    /**
     * 获取停止JUMP的分数
     */
    int getShutdownScore();

}
