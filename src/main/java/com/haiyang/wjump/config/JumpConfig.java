package com.haiyang.wjump.config;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * desc:跳一跳helper 配置类
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public class JumpConfig {
    private JumpConfig() {
    }

    private static Properties properties = new Properties();

    static {
        try {
            InputStream configPro = new BufferedInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream("jumpConfig.properties"));
            properties.load(configPro);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * adb执行路径
     */
    public static final String ADB_PATH = properties.getProperty("adbPath", "adb");

    /**
     * 跳跃系数，由实际距离远近修改。如远了，就改小点；近了，就改大点
     */
    public static final float JUMP_RATIO = Float.valueOf(properties.getProperty("jumpRatio", "1.390"));

    /**
     * 截图保存目录
     */
    public static final String SCREEN_SHOT_PATH = properties.getProperty("screenShotPath", "imgs/screenShot");

    /**
     * 调试图片输出目录
     */
    public static final String JUMP_IMG_DEBUG_PATH = properties.getProperty("jumpImgDebugPath", "imgs/jumpImgDebug");

    /**
     * 期望分数，默认为666
     */
    public static final Integer EXPECT_SCORE = Integer.valueOf(properties.getProperty("expectScore", "666"));
}
