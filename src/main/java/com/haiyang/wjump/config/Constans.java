package com.haiyang.wjump.config;

/**
 * @author haiyangp
 * date:  2018/1/13
 * desc: 常量
 */
public class Constans {
    private Constans() {
    }

    /**
     * adb命令---设备列表
     */
    public static final String CMD_ADB_DEVICE_LIST = "devices";

    /**
     * adb命令---截图,并保存到手机的sdcard/screenshot.png位置
     */
    public static final String CMD_ADB_SCREENCAP = " shell /system/bin/screencap -p /sdcard/screenshot.png";

    /**
     * adb命令---拉取截图文件
     */
    public static final String CMD_ADB_PULL_SCREENSHOT = " pull /sdcard/screenshot.png ";

    /**
     * adb命令---滑动屏幕
     */
    public static final String CMD_ADB_SWIPE = " shell input swipe %d %d %d %d %d";


}
