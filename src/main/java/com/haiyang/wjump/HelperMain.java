package com.haiyang.wjump;

import com.haiyang.wjump.config.JumpConfig;
import com.haiyang.wjump.core.AdbTool;
import com.haiyang.wjump.core.JumpHelper;
import com.haiyang.wjump.strategy.JumpStrategy;
import com.haiyang.wjump.strategy.NormalJumpStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * desc: 跳一跳--辅助入口
 *
 * @author haiyangp
 * date:   2018/1/12
 */
public class HelperMain {
    private static Logger logger = LoggerFactory.getLogger(HelperMain.class);

    public static void main(String[] args) {
        logger.debug("before run");
        logger.info("adbPath:{}", JumpConfig.ADB_PATH);
        try {
            if (!AdbTool.getInstance(JumpConfig.ADB_PATH).hasOnlineDevice()) {
                throw new RuntimeException("获取设备列表失败，请检查是否正确连接了ADB");
            }
            logger.info("设备连接成功");
            JumpHelper jumpHelper = new JumpHelper(JumpConfig.ADB_PATH, JumpConfig.JUMP_RATIO);
            JumpStrategy jumpStrategy = new NormalJumpStrategy(JumpConfig.EXPECT_SCORE);
            jumpHelper.autoJump(jumpStrategy);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
